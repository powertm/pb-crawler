"""Pastebin crawler."""
import time

import helpers

from lxml import html

import requests as req

from tinydb import TinyDB


class PastebinCrawler():
    """Crawler."""

    base_url = 'https://pastebin.com'
    paste_id_boundry = ''

    def __init__(self):
        """"Init."""
        self.db = self.init_db()

    def crawl(self):
        """Collect links, then crawl them."""
        page_content = req.get(self.base_url + '/archive').content
        page_content = html.fromstring(page_content)

        try:
            pastes_table = page_content.xpath('//table[@class="maintable"]')[0]
        except IndexError:
            # Pastebin hates us now
            return False

        paste_links = pastes_table.xpath('.//tr/td/a')

        for i, paste_link in enumerate(paste_links):

            try:

                paste_link = paste_link.attrib['href']

                if paste_link == self.paste_id_boundry:
                    break

                paste = req.get(self.base_url + paste_link).content
                paste = html.fromstring(paste)
                paste = helpers.format_paste(paste, paste_link)
                self.db.insert(paste)

            except Exception:
                continue

        self.paste_id_boundry = paste_links[0].attrib['href']

    def init_db(self):
        """Initiate DB."""
        try:
            db = TinyDB('./pastes.json')
        except Exception:
            db = open('./pastes.json', 'w+').close()
            db = TinyDB('./pastes.json')
        return db

crawler = PastebinCrawler()

while True:
    print('\nCrawling executed...')
    crawler.crawl()
    time.sleep(120)
