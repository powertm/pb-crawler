"""Helpers for Cralwer class."""
import arrow

import json


def format_paste(paste, paste_link):
    """Format a paste data into a paste model."""
    header_info = paste.xpath('.//div[@class="paste_box_info"]')[0]
    details = header_info.xpath('.//div[@class="paste_box_line2"]')[0]

    title = header_info.xpath('.//div[@title]/h1/text()')[0]
    title = '' if title == 'Untitled' else title

    author = details.xpath('.//text()')[1]
    author = author.strip()
    author = '' if author == 'a guest' else author

    date = details.xpath('.//span')[0].attrib['title']
    date = format_date(date)

    content = paste.xpath('.//textarea[@class="paste_code"]/text()')[0].strip()
    content = json.dumps(content)

    formatted_paste = {
        'author': author,
        'title': title,
        'content': content,
        'date': date,
        'paste_id': paste_link
    }

    return formatted_paste


def format_date(date):
    """Format a paste date into UTC."""
    breakdown = date.split()
    breakdown[7] = '-06:00'  # arrow is unable to parse 'CDT' as timezone.
    # (I know this isn't the most reliable way to solve this).

    del breakdown[2]  # Remove the word 'of'

    date = ' '.join(breakdown)
    date = arrow.get(date, 'dddd Do MMMM YYYY HH:mm:ss A ZZ')

    return date.to('UTC').format()
