### Setup Instructions:

##### If running from Docker:
- $ docker pull infectedmushroom/pb-crawler
- $ docker run infectedmushroom/pb-crawler

##### If running directly from local machine:
- Navigate to a path of your liking (e.g. $ cd /var/tmp/) and clone this repo: $ git clone git@bitbucket.org:powertm/pb-crawler.git
- $ cd pb-crawler
- $ python3 -m venv crawler-env
- $ source crawler-env/bin/activate
- $ pip install tinydb arrow requests lxml
- $ python3 crawl.py

Explore pastes.json to view all fetched pastes

